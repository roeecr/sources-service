import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SourceSchema } from './schemas/source.schema';
import { SourcesService } from './sources.service';
import { SourcesController } from './sources.controller';

@Module({
    imports: [MongooseModule.forFeature([{name: 'Source', schema: SourceSchema}])],
    providers: [SourcesService],
    controllers: [SourcesController],
    exports: [SourcesService]
})
export class SourcesModule {}
