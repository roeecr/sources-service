import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Delete,
  Put,
  NotFoundException,
  Query,
} from '@nestjs/common';
import { SourcesService } from './sources.service';
import { SourceDTO } from './dto/create.dto';
import { isNullOrUndefined } from 'util';

@Controller('sources')
export class SourcesController {
  constructor(private sourcesService: SourcesService) {}

  // 1
  @Get()
  async getAllSources() {
    console.log('get all sources');
    let s = await this.sourcesService.getAllSources();
    if (s.length === 0) {
      throw new NotFoundException('No sources found');
    }
    return s;
  }

  // 2
  @Get('/:uri')
  async getSourceByUri(@Param('uri') uri) {
    console.log('get source by uri');
    let s = await this.sourcesService.getSourceByUri(uri);
    if (isNullOrUndefined(s)) {
      throw new NotFoundException('Source not found');
    }
    return s;
  }

  // 3
  @Post()
  createSource(@Body() sourceDto: SourceDTO) {
    console.log('create source');
    console.log(
      'creating source:',
      sourceDto.uri,
      sourceDto.title,
      sourceDto.description,
    );
    return this.sourcesService.createSource(sourceDto);
  }

  // 4
  @Delete('/:uri')
  async deleteSource(@Param('uri') uri) {
    console.log('delete source by uri', uri);
    let s = await this.sourcesService.deleteSourceByUri(uri);
    console.log(s);
    if (isNullOrUndefined(s)) {
      throw new NotFoundException('Source not found');
    }
    return s;
  }

  // 4
  // @Delete('/:title')
  // async deleteSource(@Param('title') title) {
  //   console.log(`delete source by title:${title}!`);
  //   let s = await this.sourcesService.deleteSourceByTitle(title);
  //   console.log(s);
  //   if (isNullOrUndefined(s)) {
  //     throw new NotFoundException('Source not found');
  //   }
  //   return s;
  // }

  // 5
  // @Delete()
  // async deleteAll() {
  //     console.log('delete all sources');
  //     let s = await this.sourcesService.deleteAllSources();
  //     if(s.n === 0){
  //         throw new NotFoundException("No sources found");
  //     }
  //     return s;
  // }
  @Delete()
  async deleteManySources(@Query() query) {
    console.log('delete some sources');
    console.log(query.source);

    let ok = 0;

    let sourceArr: Promise<any>[] = [];
    for (const uri of query.source) {
      sourceArr.push(this.sourcesService.deleteSourceByUri(uri));
    }
    let a = await Promise.all(sourceArr);

    a.forEach(x => {
      ok = x.ok ? 1 : ok;
    });

    console.log('status', ok);
    return ok;
  }

  // 6
  @Put('/:uri')
  async updateSource(@Param('uri') uri, @Body() body) {
    console.log('update source by uri', uri);
    let s = await this.sourcesService.updateSource(uri, body);
    if (isNullOrUndefined(s)) {
      throw new NotFoundException('Source not found');
    }
    return s;
  }

  // 7
  // @Put()
  // async updateAllSources(@Body() body) {
  //     console.log('update all sources');
  //     let s = await this.sourcesService.updateAllSources(body);
  //     if(s.nModified === 0){
  //         throw new NotFoundException("No sources found");
  //     }
  //     return s;
  // }
  @Put()
  async updateManyUsersPassword(@Body() body) {
    console.log('update all sources info');
    console.log(body);
    console.log(body.uri);
    let answer = { n: 0, nModified: 0, ok: 0 };

    let sourceArr: Promise<any>[] = [];
    for (const uri of body.sources) {
      sourceArr.push(
        this.sourcesService.updateSource(uri, {
          title: body.title,
          description: body.description,
        }),
      );
    }
    let a = await Promise.all(sourceArr);

    a.forEach(x => {
      answer.n += x.n;
      answer.nModified = x.nModified;
      answer.ok = x.ok ? 1 : answer.ok;
    });

    console.log('answer', answer);
    return answer;
  }
}
