
export class SourceDTO {
    public uri: String;
    public title: String;
    public description: String;
}